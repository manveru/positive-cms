# frozen_string_literal: true

require 'roda'
require 'date'
require './parser'
require 'English'
require 'json'
require 'fileutils'
require 'scrypt'
require 'logger'

# App
class Editor < Roda
  LOG = Logger.new($stdout)
  CMS_LOGIN_FILE = ENV.fetch('CMS_LOGIN_FILE')
  SITE_ROOT = ENV.fetch('CMS_SITE_ROOT')
  CATEGORIES = ENV.fetch('CMS_CATEGORIES').split(',')

  opts[:root] = __dir__
  plugin :multi_public,
         'images' => [File.join(SITE_ROOT, 'static/images'), {'Cache-Control' => 'max-age=86400'}, nil],
         'css'    => [File.join(__dir__, 'static/css'), {'Cache-Control' => 'max-age=86400'}, nil],
         'js'     => [File.join(__dir__, 'static/js'), {'Cache-Control' => 'max-age=86400'}, nil],
         'font'   => [File.join(__dir__, 'static/font'), {'Cache-Control' => 'max-age=31536000'}, nil]

  plugin :render
  plugin :head
  plugin :sessions, secret: ENV.fetch('CMS_SESSION_SECRET')
  plugin :flash
  plugin :json
  plugin :r

  plugin :not_found do
    'Where did it go?'
  end

  DICT = {
    'ja' => {
      'Account' => 'アカウント',
      'Actions' => '作業',
      'Blog' => 'ブログ',
      'Change Password' => 'パスワード変更',
      'Close' => 'キャンセル',
      'Confirm New Password' => '新パスワード 再入力',
      'Created Post' => '新しい記事が作成されました',
      'Create post' => '保存',
      'Create' => '追加',
      'Current Password' => 'パスワード',
      'Date' => '日付',
      'Deleted image' => '画像が削除されました',
      'Deleted Post' => '記事が削除されました',
      'Delete' => '削除',
      'Do you really want to delete this Post?' => '本当に削除しますか？',
      'Draft' => '非公開',
      'Edit' => '編集',
      'Email address or Password is not correct' => 'IDまたはパスワードが間違っています',
      'Email address' => 'メールアドレス',
      'Info' => 'お知らせ',
      'Login' => 'ログイン',
      'Logout' => 'ログアウト',
      'New Password' => '新パスワード',
      'Password changed' => 'パスワードが変更されました',
      "Password confirmation doesn't match" => '一致しません',
      'Password' => 'パスワード',
      'Please enter your current password correctly' => 'パスワードが違います',
      'Please sign in' => 'ログイン',
      '© Positive Company LLC' => '© Positive Company LLC',
      'Private' => '非公開',
      'Public' => '公開',
      'Published' => '更新しました',
      'Publishing failed' => 'もうしばらくお待ちください',
      'Publish' => '更新',
      'Saved Post' => '記事が保存されました',
      'Save post' => '保存',
      'Set Password' => 'パスワード変更保存',
      'Status' => '非公開／公開',
      'Title' => 'タイトル',
      'Uploads' => '画像'
    }
  }.freeze

  def t(word)
    case language
    when 'en'
      word
    else
      DICT[language].fetch(word, "!#{word}!")
    end
  end

  def fix_newlines(str)
    str.gsub(/([^\n])\n([^\n])/m, "\\1  \n\\2")
  end

  def parse_content(content)
    md = []
    JSON.parse(content).fetch('ops').each do |op|
      pp op
      case op['insert']
      when String
        md << fix_newlines(op['insert'])
      when Hash
        if (image = op.dig('insert', 'image'))
          alt = op.dig('attributes', 'alt')
          md << "![#{alt}](#{image})\n\n"
        else
          pp op
        end
      else
        pp op
      end
    end
    md
  end

  def save_post(params)
    date = Date.parse(params.fetch('date'))
    title = params.fetch('title')
    draft = params['draft'] == 'on'
    category = params.fetch('category')
    name = params.fetch('name')
    content = params.fetch('content')

    dir = File.join(SITE_ROOT, 'content', category)
    FileUtils.mkdir_p(dir)

    old_path = File.join(dir, "#{name}.md")
    new_path = File.join(dir, "#{date}.md")
    tmp_path = "#{new_path}.tmp"
    FileUtils.mv(old_path, new_path) if old_path != new_path

    File.open(tmp_path, 'w+:utf-8') do |io|
      io.puts '---'
      io.puts "date: #{date}"
      io.puts "title: #{title}"
      io.puts "draft: #{draft}"
      io.puts '---'
      io.puts ''
      io.puts content.strip
    end

    FileUtils.mv(tmp_path, new_path)

    "#{category}/#{date}"
  end

  def create_post(params)
    date = Date.parse(params.fetch('date'))
    title = params.fetch('title')
    draft = params['draft'] == 'on'
    category = params.fetch('category')
    content = params.fetch('content')

    dir = File.join(SITE_ROOT, 'content', category)
    FileUtils.mkdir_p(dir)

    new_path = File.join(dir, "#{date}.md")
    tmp_path = "#{new_path}.tmp"
    File.open(tmp_path, 'w+:utf-8') do |io|
      io.puts '---'
      io.puts "date: #{date}"
      io.puts "title: #{title}"
      io.puts "draft: #{draft}"
      io.puts '---'
      io.puts ''
      io.puts content.strip
    end

    FileUtils.mv(tmp_path, new_path)

    "#{category}/#{date}"
  end

  def language
    session['language'] || 'ja'
  end

  def list_posts(category)
    @category = category
    posts = Dir["#{SITE_ROOT}/content/#{category}/*.md"].map do |path|
      { path: path, post: MarkdownParser.parse(File.read(path)) }
    end
    posts.sort_by { |post| post[:post][:meta][:date] }.reverse
  end

  def login!
    email, password = request.params.values_at('email', 'password')
    logins = JSON.parse(File.read(CMS_LOGIN_FILE))
    hash = logins[email]

    if hash && SCrypt::Password.new(hash) == password
      session['email'] = email
      r.redirect '/'
    else
      LOG.warn "login attempt with wrong password for #{email}"
      flash['toast'] = 'Email address or Password is not correct'
      r.redirect r.referer
    end
  end

  def logout!
    session.delete 'email'
  end

  def logged_in?
    session.key?('email')
  end

  def toast!(msg)
    flash['toast'] = msg
  end

  def toast
    flash['toast']
  end

  def git_changes?
    diff = `git -C '#{SITE_ROOT}' diff --exit-code &> /dev/null`
    diff_ok = $CHILD_STATUS.success?
    status = `git -C '#{SITE_ROOT}' status --porcelain`
    status_ok = $CHILD_STATUS.success?

    pp diff_ok: diff_ok, diff: diff
    pp status_ok: status_ok, status: status

    !diff_ok || (status_ok && status.lines.count.positive?)
  end

  def uploads_for(page)
    @page = page

    paths = Dir.glob("#{SITE_ROOT}/static/images/uploads/*").sort_by do |path|
      File.mtime(path)
    end.reverse

    @total_uploads = paths.map do |path|
      base = File.basename(path)
      {
        path: File.join(SITE_ROOT, '/images/uploads', base),
        base: base,
        pattern: %r{/images/uploads/#{Regexp.escape(base)}},
        refs: []
      }
    end

    @per_page = 9
    @pages = @total_uploads.size / @per_page
    @pages += 1 if @total_uploads.size % @per_page != 0

    @uploads = @total_uploads[(page * @per_page)...((page + 1) * @per_page)]

    Dir.glob("#{SITE_ROOT}/content/{#{CATEGORIES.join(",")}/*.md") do |md|
      File.open(md) do |io|
        @uploads.each do |upload|
          upload[:refs] << md.gsub(%r{^/?content(.+)\.md}, '/edit\1') if io.grep(upload[:pattern]).any?
          io.rewind
        end
      end
    end
  end

  def editor_language
    case language
    when 'ja'
      'ja-JP'
    else
      'en-US'
    end
  end

  def password_reset!
    email = session.fetch('email')
    old = request.params.fetch('old')
    new1 = request.params.fetch('new1')
    new2 = request.params.fetch('new2')

    logins = JSON.parse(File.read(CMS_LOGIN_FILE))
    hash = logins[email]

    if new1 == new2
      if hash && SCrypt::Password.new(hash) == old
        logins[email] = SCrypt::Password.create(new1)
        File.write(CMS_LOGIN_FILE, JSON.pretty_unparse(logins))
        toast! 'Password changed'
        r.redirect '/'
      else
        toast! 'Please enter your current password correctly'
        r.redirect r.referer
      end
    else
      toast! "Password confirmation doesn't match"
      r.redirect r.referer
    end
  end

  route do |r|
    r.on %w[js css font] do |dir|
      r.multi_public dir
    end

    r.on %w[images] do |dir|
      r.multi_public dir
    end

    r.get 'favicon.ico' do
      File.read(File.join(__dir__, 'static/favicon.ico'))
    end

    r.get 'login' do
      render 'login'
    end

    r.post 'login' do
      login!
    end

    r.get 'language', String do |lang|
      session['language'] = lang if %w[en ja].include?(lang)
      r.redirect request.referer
    end

    r.redirect('/login') unless logged_in?

    r.root do
      r.redirect CATEGORIES.first
    end

    r.get 'password_reset' do
      view 'password_reset'
    end

    r.post 'password_reset' do
      password_reset!
      r.redirect request.referer
    end

    r.post 'logout' do
      logout!
      r.redirect request.referer
    end

    r.post 'publish' do
      name = File.basename(SITE_ROOT)
      pull = system('git', '-C', SITE_ROOT, 'pull')
      add = pull && system('git', '-C', SITE_ROOT, 'add', File.join(SITE_ROOT, 'content'),
                           File.join(SITE_ROOT, 'static/images/uploads'))
      commit = add && system('git', '-C', SITE_ROOT, 'commit', '-m', "Published at #{Time.now}")
      push = commit && system('git', '-C', SITE_ROOT, 'push')
      build = push && system('nix', 'build', "#{SITE_ROOT}#site.tarball", '-o', "/tmp/#{name}.tar.xz")

      if build
        pp published: session
        toast! 'Published'
      else
        pp "Publishing failed: pull=#{pull} add=#{add} commit=#{commit} push=#{push} build=#{build}"
        toast! 'Publishing failed'
      end

      r.redirect r.referer
    end

    r.post 'upload' do
      image = request.params.fetch('image')
      filename = image[:filename]
      ext = File.extname(filename)
      base = File.basename(filename, ext)
      dest = nil
      1.upto 100 do |i|
        dest = File.join(SITE_ROOT, 'static/images/uploads', filename)
        filename = "#{base}-#{i}#{ext}" if File.file?(dest)
      end

      File.open(dest, 'w+:binary') do |out|
        IO.copy_stream(image[:tempfile], out)
      end

      { data: { url: "/images/uploads/#{filename}" } }
    end

    r.get 'uploads', Integer do |page|
      uploads_for(page)
      view 'uploads'
    end

    r.post 'uploads/delete', String do |name|
      root = File.expand_path(File.join(SITE_ROOT, 'static/images/uploads'))
      dest = File.expand_path(File.join(SITE_ROOT, 'static/images/uploads', name))
      if File.dirname(dest) == root
        toast! 'Deleted image'
        FileUtils.rm_f dest
      end
      r.redirect r.referer
    end

    CATEGORIES.each do |category|
      r.get category do
        @posts = list_posts(category)
        view category
      end
    end

    r.get 'create', String do |category|
      @category = category
      view 'create'
    end

    r.post 'create' do
      new_path = create_post(request.params)
      toast! 'Created Post'
      r.redirect "edit/#{new_path}"
    end

    r.post 'delete', String, String do |category, name|
      FileUtils.rm_f(File.join(SITE_ROOT, 'content', category, "#{name}.md"))
      toast! 'Deleted Post'
      r.redirect "/#{category}"
    end

    r.get 'edit', String, String do |category, name|
      @file = MarkdownParser.parse(
        File.read(File.join(SITE_ROOT, 'content', category, "#{name}.md"))
      )

      @category = category
      @name = name
      view 'edit'
    end

    r.post 'save' do
      new_path = save_post(request.params)
      toast! 'Saved Post'
      r.redirect "edit/#{new_path}"
    end

    r.post 'toggle', String, String do |category, name|
      @file = MarkdownParser.parse(
        File.read(File.join(SITE_ROOT, 'content', category, "#{name}.md"))
      )

      save_post(
        'date' => @file[:meta][:date].to_s,
        'title' => @file[:meta][:title],
        'draft' => @file[:meta][:draft] ? 'off' : 'on',
        'category' => category,
        'name' => name,
        'content' => @file[:source]
      )

      r.redirect request.referer
    end
  end
end
