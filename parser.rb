# frozen_string_literal: true

require 'redcarpet'
require 'yaml'
require 'date'
require 'English'

# Foo
class MarkdownParser
  def self.parse(str)
    s = StringScanner.new(str)

    front = teaser = nil

    # only consider the start of the file for front-matter
    if s.scan(/---\n/)
      front = s.scan_until(/---\n/)
      front_match = s.matched
      teaser = s.scan_until(/<!--\s*more\s*-->/)
      teaser_match = s.matched
      s.unscan if teaser
    end

    markdown_source = s.rest

    markdown = Redcarpet::Markdown.new(
      Redcarpet::Render::HTML,
      tables: true,
      autolink: true,
      space_after_headers: true,
      fenced_code_blocks: true,
      with_toc_data: true
    )

    result = { teaser: '', meta: {}, source: markdown_source }

    if front
      yaml = "---\n#{front[0..-(front_match.size + 1)]}"
      meta = YAML.safe_load(yaml, [Date, Time], [], true) if front
      meta.transform_keys!(&:to_sym)

      if meta[:title]
        meta[:slug] ||= meta[:title].strip.gsub(
          %r([;/?:@=&"<>#%{}|\\^~\[\]]+), '-'
        )
      end

      meta[:date] = meta[:date].to_date if meta[:date]

      result[:meta] = meta
    end

    if teaser
      md = teaser[0..-(teaser_match.size + 1)].strip
      result[:teaser] = markdown.render(md)
    end

    result[:body] = markdown.render(
      markdown_source.gsub(/<!--\s*more\s*-->/, '')
    )

    result
  end
end
