# frozen_string_literal: true

require './editor'

run Editor.freeze.app
