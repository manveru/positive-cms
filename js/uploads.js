function ready(callbackFunction) {
  if (document.readyState != "loading") {
    callbackFunction(event);
  } else {
    document.addEventListener("DOMContentLoaded", callbackFunction);
  }
}

ready((event) => {
  var imageModal = document.getElementById("imageModal");
  if (!imageModal) return;
  imageModal.addEventListener("show.bs.modal", function (event) {
    var button = event.relatedTarget;
    var path = button.getAttribute("data-bs-path");
    var modalTitle = imageModal.querySelector(".modal-title");
    var modalBodyImg = imageModal.querySelector(".modal-body img");

    modalTitle.textContent = path;
    modalBodyImg.src = path;
  });
});
