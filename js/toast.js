function ready(callbackFunction) {
  if (document.readyState != "loading") {
    callbackFunction(event);
  } else {
    document.addEventListener("DOMContentLoaded", callbackFunction);
  }
}

ready((event) => {
  var toastElList = [].slice.call(document.querySelectorAll(".toast"));
  var toastList = toastElList.map(function (toastEl) {
    var toast = new bootstrap.Toast(toastEl, {});
    toast.show();
  });
});
