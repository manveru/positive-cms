{ cms-ruby, deno, mkShell }:
mkShell { buildInputs = [ cms-ruby cms-ruby.wrappedRuby deno ]; }
