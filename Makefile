files = static/js/toast.bundle.js static/js/uploads.bundle.js static/js/editor.bundle.js

all: $(files)

clean: 
	rm -f $(files)

static/js/%.bundle.js: js/%.js
	deno fmt $<
	deno bundle --no-check $< $@
