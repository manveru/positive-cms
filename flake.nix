{
  description = "Flake for the Positive CMS";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixpkgs-unstable";
    flake-utils.url = "github:kreisys/flake-utils";
    euphenix.url = "gitlab:manveru/euphenix/flake";
  };

  outputs = { self, euphenix, flake-utils, nixpkgs }:
    let
      pkgs = import nixpkgs { system = "x86_64-linux"; };
      cms-ruby = pkgs.bundlerEnv {
        ruby = pkgs.ruby_3_0;
        name = "cms-gems";
        gemdir = ./.;
      };
      rubocop = pkgs.rubocop.override { ruby = cms-ruby.ruby; };

      positive-cms =
        pkgs.writeShellScriptBin "positive-cms" ''
          set -euo pipefail

          export PATH="${
            pkgs.lib.makeBinPath (with pkgs; [
              cms-ruby
              cms-ruby.wrappedRuby
              deno
              coreutils
              git
              openssh
              nixUnstable
            ])
          }"

          pushd ${./.}
          exec thin -R config.ru -a "''${HOST:-127.0.0.1}" -p "''${PORT:-9393}" start
        '';

    in {
      packages.x86_64-linux.positive-cms = positive-cms;
      defaultPackage.x86_64-linux = positive-cms;

      devShell.x86_64-linux = pkgs.mkShell {
        buildInputs = with pkgs; [ cms-ruby cms-ruby.wrappedRuby deno rubocop ];
      };
    };
}
